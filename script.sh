#filename="$1"

#m1=$(md5sum "$filename")

#while true; do

  # md5sum is computationally expensive, so check only once every 10 seconds
#  sleep 10

#  m2=$(md5sum "$filename")

#  if [ "$m1" != "$m2" ] ; then
#    echo "ERROR: File has changed!" >&2 
#    pwd
#    echo "Done"
#    exit 1
#  fi
#done

#!/bin/bash

if [ $1 -nt $2 ]; then
  echo "File 1 is newer than file 2"
  cp $1 $2
  ./$2
else
  echo "File 1 is older than file 2"
fi


